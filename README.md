# README #

This program simply searches for .png and .jpeg files within a folder, and then transforms it into redscale.

The blue and green values of each pixel will be subtracted out, leaving only the red. The image is then saved with the tag "Red".

The original image will be left alone.

### How to use ###

1. Go into the downloads tab on the side bar to get the executable.
1. Perform the glucose drop test as usual. Take pictures and save into one folder.
2. Run the executable in the folder with the pictures.
3. Enjoy your new red-scale only images.