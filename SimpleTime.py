from PIL import Image
import glob

imgs = glob.glob('*.png')
imgs += glob.glob('*.jpeg')

for i in imgs:
    if 'Red' in i:
        continue

    img = Image.open(i)

    size = img.size
    for x in range(size[0]):
        for y in range(size[1]):
            r = img.getpixel((x, y))
            img.putpixel((x, y), (r[0], 0, 0))

    img.save(f'Red_{i}.png')
